#!/bin/sh
set -o xtrace
set -o errexit
set -o pipefail

DEPS=" \
    curl \
    imagemagick \
    nginx \
    nginx-mod-http-image-filter \
    php7 \
    php7-apcu@testing \
    php7-bcmath \
    php7-ctype \
    php7-curl \
    php7-dom \
    php7-fpm \
    php7-gd \
    php7-gmagick@testing \
    php7-intl \
    php7-json \
    php7-libsodium@testing \
    php7-mbstring \
    php7-mcrypt \
    php7-mysqli@qluster \
    php7-mysqlnd@qluster \
    php7-opcache \
    php7-openssl \
    php7-pdo_mysql@qluster \
    php7-phar \
    php7-posix \
    php7-pspell \
    php7-redis@testing \
    php7-soap \
    php7-sockets \
    php7-xdebug \
    php7-xml \
    php7-xmlreader \
    php7-xsl \
    php7-zip \
    php7-zlib \
    yaml \
"
BUILD_DEPS="alpine-sdk php7-dev autoconf cmake yaml-dev"

# Symlink PHP7
ln -s ./php7 /usr/bin/php
ln -s ./php-fpm7 /usr/bin/php-fpm

apk add --update ${DEPS} ${BUILD_DEPS}

adduser -D -h /home/http http
mkdir -p /srv/http
chown -R http:http /srv/http

# php7-yaml
cd /tmp
git clone https://github.com/php/pecl-file_formats-yaml.git yaml
cd yaml
git checkout php7
phpize7
./configure --with-php-config=$(which php-config7)
make install

# Cleanup
apk del ${BUILD_DEPS}
rm -rf /tmp/*
rm -rf /var/cache/apk/*

rm /$0
