FROM registry.gitlab.com/qluster/alpine-daemon:edge

COPY configure.sh /configure.sh
RUN /bin/sh /configure.sh

COPY templates /templates

EXPOSE 80